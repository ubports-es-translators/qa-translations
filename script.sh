fullfilename=$1
filename=$(basename "$fullfilename")
fname="${filename%.*}"

pandoc -f latex -t odt -o ${fname}.odt ${filename}
detex "${filename}" > "${fname}.txt"
pdflatex $filename
